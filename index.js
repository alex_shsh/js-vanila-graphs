import {LineGraph, CircleGraph} from "./src";

const data1 = [
    { x: 0,  y: 0 },
    { x: 1,  y: 1 },
    { x: 2,  y: 4 },
    { x: 3,  y: 9 },
    { x: 4,  y: 16, circle: {}},
    { x: 5,  y: 25 },
    { x: 6,  y: 36, circle: {} },
    { x: 7,  y: 49 },
    { x: 8,  y: 64, circle: {} },
    { x: 9,  y: 81 },
    { x: 10, y: 100 }
]
const data2 = [
    { x: 0,  y: 13 },
    { x: 4,  y: 14 },
    { x: 4,  y: 45, circle: {}},
    { x: 5,  y: 25 },
    { x: 5,  y: 33 },
    { x: 7,  y: 49, circle: {}},
    { x: 8,  y: 64, circle: {} },
    { x: 9,  y: 81 },
    { x: 10, y: 100 }
]
const lineGraph = new LineGraph({
    data: data1,
    root: document.getElementById('app'),
    width: 600, height: 300,
    axes: {
        x: [
            {node: '0', left: '0'},
            {node: '2', left: '20%'},
            {node: '4', left: '40%'},
            {node: '6', left: '60%'},
            {node: '8', left: '80%'},
            {node: '10', left: '100%'},
        ],
        y: [
            {node: '0', bottom: '0%'},
            {node: '50', bottom: '50%'},
            {node: '100', bottom: '100%'},
        ]
    }
})
setTimeout(() => lineGraph.update(data2), 1000)


// new CircleGraph({
//     data: { percent: 50},
//     root: document.getElementById('app'),
//     width: 600, height: 300,
// })
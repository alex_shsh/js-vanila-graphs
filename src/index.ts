import {CircleGraph} from "./CircleGraph/CircleGraph";
import {LineGraph} from "./LineGraph/LineGraph";

export {
    CircleGraph,
    LineGraph
}
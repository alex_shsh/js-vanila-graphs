import {GraphParams} from "../Graph/types";

export type Data = {
    x: number,
    y: number,
    circle?: {
        setCustomCircle?: (x, y) => void,
        setPopUpText?: HTMLElement | string,
    },
}
export type LineGraphParams = {
    data: Data[],
} & GraphParams
import {LineGraphParams} from "./types";
import {Graph} from "../Graph/Graph";


export class LineGraph extends Graph {
    points: LineGraphParams['data']

    constructor({data, ...rest}: LineGraphParams) {
        super(rest)
        this.setData(data)
    }

    setData(data: LineGraphParams['data']){
        this.points = this.scalePointsData(data)
        this.mountPolyline()
        this.fillBackground()
        this.createCircles()
    }

    update(data: LineGraphParams['data']){
        this.svg.innerHTML = ''
        this.setData(data)
    }

    scalePointsData(data: LineGraphParams['data']){
        function scale(domain, range) {
            const m = (range[1] - range[0]) / (domain[1] - domain[0]);
            return num => range[0] + m * (num - domain[0]);
        }
        const x = scale([0, Math.max(...data.map(d => d.x))], [0, this.width]);
        const y = scale([0, Math.max(...data.map(d => d.y))], [this.height, 0]);
        return data.map(d => ({...d, x: x(d.x), y: y(d.y)}))
    }

    fillBackground(){
        this.createPath(
            `M 0 ${this.height} ${this.points.map(el => `L ${(el.x)} ${(el.y)}`).join(' ')} ${this.width} ${this.height} z`,
            `rgba(209, 62, 62, 0.55)`
        )
    }

    mountPolyline(color = 'red'){
        const polyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
        polyline.setAttribute('fill', `none`);
        polyline.setAttribute('stroke', color);
        polyline.setAttribute('points', this.points.map(el => `${(el.x)},${(el.y)}`).join(' '));
        this.svg.appendChild(polyline)
    }

    createCircles(color = 'black'){
        const group = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        this.points.forEach(el => {
            if(el.circle?.setCustomCircle) el.circle?.setCustomCircle(el.x, el.y)
            else if(el.circle) {
                const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
                circle.setAttribute('cx', `${el.x}`);
                circle.setAttribute('cy', `${el.y}`);
                circle.setAttribute('r', '5');
                circle.setAttribute('fill', color);
                circle.setAttribute('style', 'transition: all 0.5s;');
                circle.addEventListener('mouseenter', this.handleMouseEnterCircle.bind(this))
                circle.addEventListener('mouseleave', this.handleMouseLeaveCircle.bind(this))
                group.appendChild(circle)
            }
        })
        this.svg.appendChild(group)
    }

    handleMouseEnterCircle(e: MouseEvent){
        const target = e.target as SVGCircleElement;
        target.setAttribute('r', '10');
    }

    handleMouseLeaveCircle(e: MouseEvent){
        const target = e.target as SVGCircleElement;
        target.setAttribute('r', '5');
    }
}
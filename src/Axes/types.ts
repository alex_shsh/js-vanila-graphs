export type AxisBase = {
    node: string | HTMLElement
}
export type AxisX = {
    left?: string,
} & AxisBase
export type AxisY = {
    bottom?: string,
} & AxisBase

export enum AxesEnum {
    HTML = 'HTML',
    SVG = 'SVG',
}
export type Axes = {
    x: AxisX[],
    y: AxisY[],
    type?: AxesEnum
}

export type IAxis = {
    mount(): void,
}

//  AxisHtml
export type AxisHtmlParams = {
    axes: Axes,
    root: HTMLElement,
    width: number;
    height: number;
}
//  AxisSVG
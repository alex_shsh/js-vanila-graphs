import {Axes, AxisHtmlParams, IAxis} from "./types";
import {AxisX, AxisY} from "./types";

export class AxisHtml implements IAxis{
    axes: Axes
    root: HTMLElement
    width: number
    height: number

    constructor({axes, root, width, height}: AxisHtmlParams) {
         this.axes = axes
         this.root = root
         this.width = width
         this.height = height
    }

    // Оси можно сделать и в svg https://codepen.io/Jackel27/details/Kzqxjo
    mount(){
        if(!this.axes) return;
        let x = document.createElement('div')
        let y = document.createElement('div')
        x.classList.add('x', 'axis')
        y.classList.add('y', 'axis')
        x.setAttribute('style', `max-width: ${this.width}px`)
        y.setAttribute('style', `max-height: ${this.height}px`)

        const setAxis = <T extends AxisY & AxisX>(parent: HTMLElement, axis: T[], styleCallback: (el: T) => string) => {
            axis.forEach(el => {
                let node = document.createElement('span')
                if(typeof el.node === 'string') node.innerHTML = el.node
                else node.appendChild(el.node)
                node.setAttribute('style', styleCallback(el))
                parent.appendChild(node)
            })
        }
        setAxis(x, this.axes.x, (el) => `left: ${el.left}`)
        setAxis(y, this.axes.y, (el) => `bottom: ${el.bottom}`)

        this.mountStyleTag()

        this.root.style.position = 'relative'
        this.root.style.height = `${this.height}px`
        this.root.appendChild(x)
        this.root.appendChild(y)
    }

    mountStyleTag(){
        let head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');
        let css = `
        .axis {
            position: absolute;
        }
        .axis span {
            position: absolute;
            line-height: 1;
        }
        .x.axis {
            top: 100%; width: 100%; border-top: 3px solid black;
        }
        .y.axis {
            height: 100%; border-left: 3px solid black; bottom: 0;
        }
        .x.axis span {
            top: 0.5em;
            transform: translate(-50%,0);
        }
        .y.axis span {
            left: -0.5em;
            transform: translate(-100%,50%);
        }
        `

        style.appendChild(document.createTextNode(css));
        head.appendChild(style);
    }
}
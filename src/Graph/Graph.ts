 import {AxesEnum, IAxis} from "../Axes/types";
import {AxisHtml} from "../Axes/AxisHtml";
import {GraphParams} from "./types";

export class Graph {
    width: GraphParams['width']
    height: GraphParams['height']
    root: GraphParams['root']
    svg: SVGElement
    axes: IAxis

    constructor({root, width, height, axes}: GraphParams) {
        this.root = root
        this.width = width
        this.height = height
        this.mountSvg()
        this.mountAxes(axes)
    }

    mountSvg(){
        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        svg.setAttribute('width', `${this.width}px`);
        svg.setAttribute('height', `${this.height}px`);
        svg.setAttribute('viewBox', `0 0 ${this.width} ${this.height}`);
        if(this.root) {
            this.svg = svg
            this.root.appendChild(svg)
        }
        else throw new Error(`Parameter root does not exist: ${this.root}`)
    }

    mountAxes(axes: GraphParams['axes']){
        if(axes) {
            if(axes.type === AxesEnum.SVG){
                // this.Axes = new AxisHtml({Axes, root, width: this.width, height: this.height})
            } else {
                this.axes = new AxisHtml({axes, root: this.root, width: this.width, height: this.height})
            }

            this.axes.mount()
        }
    }


    // https://svg-art.ru/?page_id=897
    createPath(pathString = '', color = 'black'){
        const path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        path.setAttribute('fill', `rgba(209, 62, 62, 0.55)`);
        path.setAttribute('d', pathString);
        this.svg.appendChild(path)
    }
}
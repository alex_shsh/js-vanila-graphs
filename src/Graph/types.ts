import {Axes} from "../Axes/types";


export type GraphParams = {
    root?: HTMLElement,
    width: number;
    height: number;
    axes?: Axes
}

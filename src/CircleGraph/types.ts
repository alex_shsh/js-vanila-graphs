import {GraphParams} from "../Graph/types";

export type CircleGraphData = {
    percent: number,
}

export type CircleGraphParams = {
    data: CircleGraphData,
} & GraphParams
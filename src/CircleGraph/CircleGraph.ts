import {Graph} from "../Graph/Graph";
import {CircleGraphParams} from "./types";


export class CircleGraph extends Graph {
    data: CircleGraphParams['data']

    constructor({data, ...rest}: CircleGraphParams) {
        super(rest)
        this.data = data
        this.mountCircle()
    }

    mountCircle(r = 100){
        const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        circle.setAttribute('fill', `#ddd`);
        circle.setAttribute('r', `${r}`);
        circle.setAttribute('cx', `${this.width / 2}`);
        circle.setAttribute('cy', `${this.height / 2}`);
        // circle.setAttribute('stroke-width', '200');
        // circle.setAttribute('stroke', 'tomato');
        // circle.setAttribute('stroke-dasharray', '25 100');
        this.svg.appendChild(circle)
    }
}
// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"src/Axes/types.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AxesEnum = void 0;
var AxesEnum;

(function (AxesEnum) {
  AxesEnum["HTML"] = "HTML";
  AxesEnum["SVG"] = "SVG";
})(AxesEnum = exports.AxesEnum || (exports.AxesEnum = {})); //  AxisSVG
},{}],"src/Axes/AxisHtml.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AxisHtml = void 0;

var AxisHtml =
/** @class */
function () {
  function AxisHtml(_a) {
    var axes = _a.axes,
        root = _a.root,
        width = _a.width,
        height = _a.height;
    this.axes = axes;
    this.root = root;
    this.width = width;
    this.height = height;
  } // Оси можно сделать и в svg https://codepen.io/Jackel27/details/Kzqxjo


  AxisHtml.prototype.mount = function () {
    if (!this.axes) return;
    var x = document.createElement('div');
    var y = document.createElement('div');
    x.classList.add('x', 'axis');
    y.classList.add('y', 'axis');
    x.setAttribute('style', "max-width: ".concat(this.width, "px"));
    y.setAttribute('style', "max-height: ".concat(this.height, "px"));

    var setAxis = function setAxis(parent, axis, styleCallback) {
      axis.forEach(function (el) {
        var node = document.createElement('span');
        if (typeof el.node === 'string') node.innerHTML = el.node;else node.appendChild(el.node);
        node.setAttribute('style', styleCallback(el));
        parent.appendChild(node);
      });
    };

    setAxis(x, this.axes.x, function (el) {
      return "left: ".concat(el.left);
    });
    setAxis(y, this.axes.y, function (el) {
      return "bottom: ".concat(el.bottom);
    });
    this.mountStyleTag();
    this.root.style.position = 'relative';
    this.root.style.height = "".concat(this.height, "px");
    this.root.appendChild(x);
    this.root.appendChild(y);
  };

  AxisHtml.prototype.mountStyleTag = function () {
    var head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');
    var css = "\n        .axis {\n            position: absolute;\n        }\n        .axis span {\n            position: absolute;\n            line-height: 1;\n        }\n        .x.axis {\n            top: 100%; width: 100%; border-top: 3px solid black;\n        }\n        .y.axis {\n            height: 100%; border-left: 3px solid black; bottom: 0;\n        }\n        .x.axis span {\n            top: 0.5em;\n            transform: translate(-50%,0);\n        }\n        .y.axis span {\n            left: -0.5em;\n            transform: translate(-100%,50%);\n        }\n        ";
    style.appendChild(document.createTextNode(css));
    head.appendChild(style);
  };

  return AxisHtml;
}();

exports.AxisHtml = AxisHtml;
},{}],"src/Graph/Graph.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Graph = void 0;

var types_1 = require("../Axes/types");

var AxisHtml_1 = require("../Axes/AxisHtml");

var Graph =
/** @class */
function () {
  function Graph(_a) {
    var root = _a.root,
        width = _a.width,
        height = _a.height,
        axes = _a.axes;
    this.root = root;
    this.width = width;
    this.height = height;
    this.mountSvg();
    this.mountAxes(axes);
  }

  Graph.prototype.mountSvg = function () {
    var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.setAttribute('width', "".concat(this.width, "px"));
    svg.setAttribute('height', "".concat(this.height, "px"));
    svg.setAttribute('viewBox', "0 0 ".concat(this.width, " ").concat(this.height));

    if (this.root) {
      this.svg = svg;
      this.root.appendChild(svg);
    } else throw new Error("Parameter root does not exist: ".concat(this.root));
  };

  Graph.prototype.mountAxes = function (axes) {
    if (axes) {
      if (axes.type === types_1.AxesEnum.SVG) {// this.Axes = new AxisHtml({Axes, root, width: this.width, height: this.height})
      } else {
        this.axes = new AxisHtml_1.AxisHtml({
          axes: axes,
          root: this.root,
          width: this.width,
          height: this.height
        });
      }

      this.axes.mount();
    }
  }; // https://svg-art.ru/?page_id=897


  Graph.prototype.createPath = function (pathString, color) {
    if (pathString === void 0) {
      pathString = '';
    }

    if (color === void 0) {
      color = 'black';
    }

    var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    path.setAttribute('fill', "rgba(209, 62, 62, 0.55)");
    path.setAttribute('d', pathString);
    this.svg.appendChild(path);
  };

  return Graph;
}();

exports.Graph = Graph;
},{"../Axes/types":"src/Axes/types.ts","../Axes/AxisHtml":"src/Axes/AxisHtml.ts"}],"src/CircleGraph/CircleGraph.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __rest = this && this.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CircleGraph = void 0;

var Graph_1 = require("../Graph/Graph");

var CircleGraph =
/** @class */
function (_super) {
  __extends(CircleGraph, _super);

  function CircleGraph(_a) {
    var _this = this;

    var data = _a.data,
        rest = __rest(_a, ["data"]);

    _this = _super.call(this, rest) || this;
    _this.data = data;

    _this.mountCircle();

    return _this;
  }

  CircleGraph.prototype.mountCircle = function (r) {
    if (r === void 0) {
      r = 100;
    }

    var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circle.setAttribute('fill', "#ddd");
    circle.setAttribute('r', "".concat(r));
    circle.setAttribute('cx', "".concat(this.width / 2));
    circle.setAttribute('cy', "".concat(this.height / 2)); // circle.setAttribute('stroke-width', '200');
    // circle.setAttribute('stroke', 'tomato');
    // circle.setAttribute('stroke-dasharray', '25 100');

    this.svg.appendChild(circle);
  };

  return CircleGraph;
}(Graph_1.Graph);

exports.CircleGraph = CircleGraph;
},{"../Graph/Graph":"src/Graph/Graph.ts"}],"src/LineGraph/LineGraph.ts":[function(require,module,exports) {
"use strict";

var __extends = this && this.__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");

    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

var __rest = this && this.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LineGraph = void 0;

var Graph_1 = require("../Graph/Graph");

var LineGraph =
/** @class */
function (_super) {
  __extends(LineGraph, _super);

  function LineGraph(_a) {
    var _this = this;

    var data = _a.data,
        rest = __rest(_a, ["data"]);

    _this = _super.call(this, rest) || this;

    _this.setData(data);

    return _this;
  }

  LineGraph.prototype.setData = function (data) {
    this.points = this.scalePointsData(data);
    this.mountPolyline();
    this.fillBackground();
    this.createCircles();
  };

  LineGraph.prototype.update = function (data) {
    this.svg.innerHTML = '';
    this.setData(data);
  };

  LineGraph.prototype.scalePointsData = function (data) {
    function scale(domain, range) {
      var m = (range[1] - range[0]) / (domain[1] - domain[0]);
      return function (num) {
        return range[0] + m * (num - domain[0]);
      };
    }

    var x = scale([0, Math.max.apply(Math, data.map(function (d) {
      return d.x;
    }))], [0, this.width]);
    var y = scale([0, Math.max.apply(Math, data.map(function (d) {
      return d.y;
    }))], [this.height, 0]);
    return data.map(function (d) {
      return __assign(__assign({}, d), {
        x: x(d.x),
        y: y(d.y)
      });
    });
  };

  LineGraph.prototype.fillBackground = function () {
    this.createPath("M 0 ".concat(this.height, " ").concat(this.points.map(function (el) {
      return "L ".concat(el.x, " ").concat(el.y);
    }).join(' '), " ").concat(this.width, " ").concat(this.height, " z"), "rgba(209, 62, 62, 0.55)");
  };

  LineGraph.prototype.mountPolyline = function (color) {
    if (color === void 0) {
      color = 'red';
    }

    var polyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
    polyline.setAttribute('fill', "none");
    polyline.setAttribute('stroke', color);
    polyline.setAttribute('points', this.points.map(function (el) {
      return "".concat(el.x, ",").concat(el.y);
    }).join(' '));
    this.svg.appendChild(polyline);
  };

  LineGraph.prototype.createCircles = function (color) {
    var _this = this;

    if (color === void 0) {
      color = 'black';
    }

    var group = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.points.forEach(function (el) {
      var _a, _b;

      if ((_a = el.circle) === null || _a === void 0 ? void 0 : _a.setCustomCircle) (_b = el.circle) === null || _b === void 0 ? void 0 : _b.setCustomCircle(el.x, el.y);else if (el.circle) {
        var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        circle.setAttribute('cx', "".concat(el.x));
        circle.setAttribute('cy', "".concat(el.y));
        circle.setAttribute('r', '5');
        circle.setAttribute('fill', color);
        circle.setAttribute('style', 'transition: all 0.5s;');
        circle.addEventListener('mouseenter', _this.handleMouseEnterCircle.bind(_this));
        circle.addEventListener('mouseleave', _this.handleMouseLeaveCircle.bind(_this));
        group.appendChild(circle);
      }
    });
    this.svg.appendChild(group);
  };

  LineGraph.prototype.handleMouseEnterCircle = function (e) {
    var target = e.target;
    target.setAttribute('r', '10');
  };

  LineGraph.prototype.handleMouseLeaveCircle = function (e) {
    var target = e.target;
    target.setAttribute('r', '5');
  };

  return LineGraph;
}(Graph_1.Graph);

exports.LineGraph = LineGraph;
},{"../Graph/Graph":"src/Graph/Graph.ts"}],"src/index.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LineGraph = exports.CircleGraph = void 0;

var CircleGraph_1 = require("./CircleGraph/CircleGraph");

Object.defineProperty(exports, "CircleGraph", {
  enumerable: true,
  get: function get() {
    return CircleGraph_1.CircleGraph;
  }
});

var LineGraph_1 = require("./LineGraph/LineGraph");

Object.defineProperty(exports, "LineGraph", {
  enumerable: true,
  get: function get() {
    return LineGraph_1.LineGraph;
  }
});
},{"./CircleGraph/CircleGraph":"src/CircleGraph/CircleGraph.ts","./LineGraph/LineGraph":"src/LineGraph/LineGraph.ts"}],"index.js":[function(require,module,exports) {
"use strict";

var _src = require("./src");

var data1 = [{
  x: 0,
  y: 0
}, {
  x: 1,
  y: 1
}, {
  x: 2,
  y: 4
}, {
  x: 3,
  y: 9
}, {
  x: 4,
  y: 16,
  circle: {}
}, {
  x: 5,
  y: 25
}, {
  x: 6,
  y: 36,
  circle: {}
}, {
  x: 7,
  y: 49
}, {
  x: 8,
  y: 64,
  circle: {}
}, {
  x: 9,
  y: 81
}, {
  x: 10,
  y: 100
}];
var data2 = [{
  x: 0,
  y: 13
}, {
  x: 4,
  y: 14
}, {
  x: 4,
  y: 45,
  circle: {}
}, {
  x: 5,
  y: 25
}, {
  x: 5,
  y: 33
}, {
  x: 7,
  y: 49,
  circle: {}
}, {
  x: 8,
  y: 64,
  circle: {}
}, {
  x: 9,
  y: 81
}, {
  x: 10,
  y: 100
}];
var lineGraph = new _src.LineGraph({
  data: data1,
  root: document.getElementById('app'),
  width: 600,
  height: 300,
  axes: {
    x: [{
      node: '0',
      left: '0'
    }, {
      node: '2',
      left: '20%'
    }, {
      node: '4',
      left: '40%'
    }, {
      node: '6',
      left: '60%'
    }, {
      node: '8',
      left: '80%'
    }, {
      node: '10',
      left: '100%'
    }],
    y: [{
      node: '0',
      bottom: '0%'
    }, {
      node: '50',
      bottom: '50%'
    }, {
      node: '100',
      bottom: '100%'
    }]
  }
});
setTimeout(function () {
  return lineGraph.update(data2);
}, 1000); // new CircleGraph({
//     data: { percent: 50},
//     root: document.getElementById('app'),
//     width: 600, height: 300,
// })
},{"./src":"src/index.ts"}],"../../../../.config/yarn/global/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "50515" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../.config/yarn/global/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","index.js"], null)
//# sourceMappingURL=/vanila-graphs.e31bb0bc.js.map